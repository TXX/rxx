
public class Keys<Type> {
	
	private String[] keys = {
		"ServerName",
		"ServerHost",
		"ServerPort",
		"ServerPassword",
		"ServerSSL",
		"BotNick",
		"BotUsername",
		"BotHostname",
		"BotRealname"
	};
		
	public void addKey(String newKeyName) {
		if(Launcher.DEBUG) {  }
		String[] temp = new String[this.keys.length + 1];
		temp = this.keys;
		temp[this.keys.length + 1] = newKeyName;
		this.keys = temp;
	}
	
	public String[] getKeys() {
		return this.keys;
	}
}
