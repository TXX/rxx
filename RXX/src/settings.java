import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/*
 * FIXME: Make settings work.
 * TODO: Test loading and saving.
 * XXX: check out file structure and perhaps optimize it.
 */

public class settings {
	
	
	private String settingsType;
	private static Properties settings;
	private Keys myKeys; // XXX: maybe move them?
	private static File fp;
	private static String defaultPath = "conf" + System.getProperty("file.separator");
	
	public settings(String filename) throws IOException {
		this.settingsType = filename;
		fp = new File(defaultPath);
		if(fp.exists()) {
			if(fp.mkdir()) {
				fp = new File(defaultPath + filename + ".rxcf");
				if(fp.exists() && fp.canWrite()) {
					try {
						fp.createNewFile();
					} catch (IOException e) {
						Debug.error("Caught Exception at " + e.getMessage());
					} finally {
						// TODO: write settings to file!
					}
				}
			} else {
				Debug.error("Can't make directory.");
			}
		}
	}
	
	public static Properties loadSettings(String filename) {
		fp = new File(defaultPath + filename + ".rxcf");
		try {
			settings.load(new FileInputStream(fp));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return settings;
	}

	public void saveSettings() {
		
	}
	
	public void addSetting() {
		
	}
	
	public void addKey() {
		
	}
}
