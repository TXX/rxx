


public class Command {
	
	
	public static int Pass(Server serv, String pass) {
		if(!pass.isEmpty()) {
			serv.print("PASS " + pass);
		}
		return 0;
	}
	
	public static void Nick(Server serv, String nick) {
		serv.print("NICK " + nick);
	}
	
	public static void User(Server serv) {
		serv.print("USER " + serv.getUsername() + " " + serv.getHostname() + " :" + serv.getRealname());
	}
	
	public static void Ping(Server serv, String host) {
		serv.print("PING " + host);
	} 
	
	public static void Pong(Server serv) {
		serv.print("PONG " + serv.getHost());
	}
	
}
