


import java.util.ArrayList;

/**
 * The center point of the program. Contains the list of servers, and the command input.
 * This class is meant to be the initializing point, but also the clean up point.
 * 
 *  WTF
 * 
 * TODO: create config file reading. (Check out default package -> settings.java)
 * @author TXX
 *
 */

public class Launcher {
	public static boolean DEBUG = false; // Global debug var, unless set later, it is false. See main for activation.
	
	private static ArrayList<Server> serverlist = null; // List of servers to be or connected to.
	
	public static void main(String[] args) {
//		CommandCenter temp = new CommandCenter();
//		System.out.println(temp.listServers());
//		
		
		if(args.toString().contains("--debug")) { // Activation of global debug.
			DEBUG = true;
			System.out.println("Debug enabled."); // For debuging of debuging is on.
		}
		
		serverlist = new ArrayList<Server>(); //instanciate the server list.
		// TEST SERVER
		serverlist.add(createDummyServer()); //FIXME: Shit's not realistic.
		
		connectAll(); // After adding all server entries, let's connect to them.
		
		//System.out.println(cc.listServers());
		
		java.util.Scanner inputTest = new java.util.Scanner(System.in);
		System.out.println("Type \'stop\' to abort the connection");
		boolean breakout = false;
		while(breakout) {
			String inp = inputTest.next();
			if(inp.equals("stop")) {
				breakout = true;
			} else {
				if(forwardCmd(inp)) {
					break;
				} else {
				System.out.println("Type \'stop\' to abort the connection");
				}
			}
		}
		//cc.shutdown();
		
	}
	
	private static Server createDummyServer() {
		Server temp = new Server("Dummy");
		
		temp.setHost("blucoders.net");
		temp.setPort(6667);
		
		temp.setNick("RXX");
		temp.setUsername("rxx");
		temp.setHostname("blucoders.net");
		temp.setRealname("Botownedby-TXX");
		
		
		return temp;
	}
	
	private static void connectAll() {
		if(serverlist.size() > 0) {
			cc = new CommandCenter();
			for(Server serv : serverlist) {
				cc.addServer(serv);
			}
		}
	}
	
	private static boolean forwardCmd(String inp) {
		if(inp.equals("test")) {
			System.out.println("Works.");
			return true;
		} else {
			return false;
		}
	}
}
