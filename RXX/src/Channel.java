

public class Channel {
	
	private String channelname = null;
	
	public Channel(String channel) {
		this.setChannelname(channel);
	}

	public String getChannelname() {
		return channelname;
	}

	public void setChannelname(String channelname) {
		this.channelname = channelname;
	}

}
