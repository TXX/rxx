


import java.util.ArrayList;



public class CommandCenter {
	
	private static final int MAXSERVERS = 10;
	private ArrayList<Connection> servers = null;
	

	public CommandCenter() {
		servers = new ArrayList<Connection>(MAXSERVERS);

	}
	
	public void addServer(Server serv) {
		if(servers.size() < MAXSERVERS) {
			Connection newConnection = new Connection(serv.getHost(), serv.getPort());
			servers.add(newConnection);
			serv.setConnection(newConnection);
			serv.setInput(newConnection.getInput());
			serv.setOutput(newConnection.getOutput());
			identify(serv);
		} else {
			if(Launcher.DEBUG) { System.out.println("TOO MANY SERVERS"); }
		}
	}
	
	private void identify(Server serv) {
		Connection.Nick(serv, serv.getNick());
		Connection.User(serv);
		// join channels
	}
	
	public void shutdown() {
		if(servers.size() > 0) {
			for(int i = 0; i < servers.size(); i++) {
				servers.get(i).close();
			}
		}
	}
	
	public String listServers() {
		String retval = "";
		
		if(servers.size() > 0 && servers.size() < MAXSERVERS) {
			retval += "--- Servers ---\n";
			
			for(int i = 0; i < servers.size(); i++) {
				String listEntry = null;
				listEntry += servers.get(i).getHostname();
				listEntry += " - [" + servers.get(i).getStatus() + "]";
				listEntry += "\n";
				retval += listEntry;
			}
		} else {
			retval = "No servers yet.";
		}
		return retval;
	}
	
}
