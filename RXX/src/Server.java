


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The server model class, contains the blueprints of a server connection.
 * @author TXX
 *
 */

public class Server {

	private String name = null; //NickName
	
	private Connection myConnection = null;
	private BufferedReader myInput = null;
	private BufferedWriter myOutput = null;
	
	/* Connection data */ 
	private String host = null;
	private int port = 6667;
	private String password = null;
	private boolean ssl = false;
	
	/* Identification data */
	private String nick = null;
	
	private String username = null;
	private String hostname = null; // same as host
	//private String servername = null; // mostly irrelevant
	private String realname = null; // Remember :
	
	
	/* Channel list */
	private ArrayList<Channel> channels = null;
	
	/* Bot owner/master */
	private Master myMaster = null;
	
	/* additional info like time on server, dc's alternative info. */
	//Time
	
	public Server(String name) {
		this.setName(name);
	}
	
	public void print(String s) {
		
		if(!s.isEmpty()) {
		
			try {
				myOutput.write(s + "\r\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			if(Launcher.DEBUG) { System.out.println("String was empty, didn't write to socket."); }
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Connection getConnection() {
		return myConnection;
	}

	public void setConnection(Connection myConnection) {
		this.myConnection = myConnection;
	}

	public BufferedReader getInput() {
		return myInput;
	}

	public void setInput(BufferedReader myInput) {
		this.myInput = myInput;
	}

	public BufferedWriter getOutput() {
		return myOutput;
	}

	public void setOutput(BufferedWriter myOutput) {
		this.myOutput = myOutput;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

//	public String getServername() {
//		return servername;
//	}
//
//	public void setServername(String servername) {
//		this.servername = servername;
//	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public ArrayList<Channel> getChannels() {
		return channels;
	}

	public void setChannels(ArrayList<Channel> channels) {
		this.channels = channels;
	}

	public Master getMyMaster() {
		return myMaster;
	}

	public void setMyMaster(Master myMaster) {
		this.myMaster = myMaster;
	}

}
