


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;



/**
 * Connection class for direct IO manipulation on per socket basis.
 * Each connection class contains a in and ouput stream linked with the server class.
 * 
 * TODO: Add threading support
 * @author TXX
 *
 */

public class Connection {
	
	private String host;
	private int port;
	
	private Socket link = null;
	private BufferedReader input = null;
	private BufferedWriter output = null;
	private PrintWriter outputWriter = null;
	
	private boolean connected = false;
	
	/* ----------------------------------------- */
		
	public Connection(String host, int port) {
		this.port = port;
		this.host = host;
		this.connect(this.lookup(this.host));

	}
	
	/* ------------------------------------------------- */
	
	/*
	 * TODO Add auto reconnect if server fails with timer etc.
	 * TODO report if server doesn't connect.
	 * TODO Make report center, that can info if connection established to one client at least, else write to log and email.
	 */
	
	private String lookup(String host) {
		
		String retval = "";
		
		try {
			InetAddress serverAddress = InetAddress.getByName(host);
			
			if(Launcher.DEBUG) { System.out.println("Resolving host.\n\tHost: " + host + "\n\tResolves to IP: " + serverAddress.getHostAddress() + "\n\tEvt hostname: " + serverAddress.getHostName()); }
			
			retval = serverAddress.getHostAddress();
			
		} catch (Exception e) {
			if(Launcher.DEBUG) {System.out.println(e.toString());}
			// TODO : Setup logging facility
			// TODO UnknownHostException 
		} 
		
		return retval;
	}
	
	private void connect(String hostAddr) {
		
		try {
			
			link = new Socket(hostAddr, this.port);
			
			System.out.println("" + this.host + "@" + hostAddr + "::" + this.port + "\nCreating I/O connections for socket.");
			
			setInput(new BufferedReader(new InputStreamReader(link.getInputStream())));
			outputWriter = new PrintWriter(link.getOutputStream());
			setOutput(new BufferedWriter(outputWriter));
			
			connected = true;
			
			if(Launcher.DEBUG) { System.out.println("Connected!"); }
			
		} catch (Exception e) {
			if(Launcher.DEBUG) { System.out.println(e.toString() + "\nConnection failed, i think."); }
		}
	}
	
	private void closeConnection() {
		try {
			link.close();
			// TODO Probably check a error stack or something. So that we can be sure we exited with no errors.
			System.out.println("Closed without problems I think.");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			connected = false;
		}
	}
	
	/* ------------------------------------------------ */
	
	public void close() {
		closeConnection();
	}


	public BufferedReader getInput() {
		return input;
	}


	public void setInput(BufferedReader input) {
		this.input = input;
	}


	public BufferedWriter getOutput() {
		return output;
	}


	public void setOutput(BufferedWriter output) {
		this.output = output;
	}
	
	public String getHostname() {
		return host;
	}
	
	public String getStatus() {
		if(connected) { return "Connected"; 
		} else { return "N/A"; }
	}
}
