

/**
 * General debug class, used to sort and kep stats of what kind of errors etc comes in.
 * It's layout is designed to support level logging to a log file, so as to keep track of what happens.
 * 
 * FIXME: Actually make it work.
 * TODO: Add logging
 * TODO: migrate global debug switch to here;
 * XXX: Add time-stamps and time control for the logging.
 * @author TXX 
 *
 */

public class Debug {
	
	private static int[] dbgStat = { 0, 0, 0};
	
	public static void error(String error) {
		dbgStat[0]++;
		transmit("Error: " + error);
	}
	
	public static void warning(String warning) {
		dbgStat[1]++;
		transmit("Warning: " + warning);
	}

	public static void info(String info) {
		dbgStat[2]++;
		transmit("Info: " + info);
	}
	
	private static void transmit(String msg) {
		//log(msg);
		System.out.println("DBG@" + msg);
	}
}
